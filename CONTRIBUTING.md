# Contributing Guidelines

1. No **updating** directly to the main branch, this will only be done by admins as appointed by Orion(GotamDahiya).
2. Create a new branch for a new post or series of posts or changing the UI of the website. The branch name should indicate what feature(s) are being worked on. Only proper nouns separated by an underscore, _**\_**_ .
3. Commit messages should be proper and for each individual file, not multiple files containing the same commit message.
4.  Merge requests are divided as per the following,
    - _Title_ : Proper sentence of the branch name
    - _Message_ : Bullet points of what all was changed, added or removed. Each file change, removal or addition should be included and explained.
    - _Review_ : Request at least one person to review the merge request so that everything is hunky dory.
5. Branches will only be deleted by admins as appointed by Orion(GotamDahiya). No other contributor should delete the branches.

For any questions, ask Orion on discord either in J5 chat or as a direct message.