+++
title = 'Campaign Artemis'
slug = 'post1'
image = 'images/Operation_Artemis_Start.jpg'
date = "2023-02-02T00:00:00"
description = 'Following the success of Campaign Onyx and the death of Achmed, Detachment 207 is going to the Kujari region (MGR 36P) to report on The Corporation Activities'
disableComments = true
+++

## Build-Up

Intel recovered from Achmed's bunker in Vidda and the nuclear explosion from a year ago point towards the region of Kujari, MGR 36P, in Africa. The Corporation which is the military arm of the organisation which Detachment 207 is pursuing is planning something big in Kujari and moving equipment and personnel through the region.

Detachment 207's task is to gather intel regarding The Corporation and assess what is happening there.

## Operation Artemis I

![Figure 1](/images/campaign_artemis/artemis_1.jpg "Detachment 207 Logi Coming In")
|:--:|
| *Detachment 207 Logi Coming In* |

Detachment 207 have been airlifted into the region's northern airfield and will be conducting an armed reconnaissance convoy through the region asking the civilians about any suspicious activity and keep an eye out on any militia or Corporation Forces.

### Aftermath

Operation Artemis 1 saw 207 encounter Kujari insurgents, attempting to destabilise the local region for an unknown reason. No Corporation activities have been discovered at this time.

## Operation Artemis II

Detachment 207 will be inspecting a dirt airstrip in the southern region of Kujari. There is known suspicious activity on the airstrip, but the ownership is a mystery. These flights may be smuggling operations for the Corporation and will expose their activities.

After securing the airfield, a sweep will be conducted for any actionable intelligence. 