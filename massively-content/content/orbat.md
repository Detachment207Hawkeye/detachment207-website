+++
title = 'Orbat'
slug = 'Orbat'
image = 'images/1.jpg'
description = 'Operational Structure of Detachment 207 all the way from Company HQ till Individual Platoons'
disableComments = true
mermaid=true
+++

## Structure

{{< mermaid >}}
flowchart TD
    id1(Company HQ, 1-0-HQ)-->id2(Support Assets);
    id1(Company HQ, 1-0-HQ)-->id3(1-1 Platoon, 1-1-HQ);
    id2(Support Assets)-->id4(Aviation, 1-5-Hawkeye);
    id2(Support Assets)-->id5(Combat Support, 1-5-F);
    id3(1-1 Platoon, 1-1-HQ)-->id6(1-1 Alpha);
    id3(1-1 Platoon, 1-1-HQ)-->id7(1-1 Bravo);
    id3(1-1 Platoon, 1-1-HQ)-->id8(1-1 Charlie);

    id9(Mission Coordination 0-A)
{{< /mermaid >}}


## Company HQ, **1-0-HQ**

| ![1-0-HQ-Logo](/images/orbat/1_COY_HQ.png) | 
|:--:|
| *1-0-HQ Logo* |

Company HQ is responsible for handling the platoon(s) as well as the support assets. They also coordinate with the Mission Coordination team to ensure a smooth flow for operations in-game. 

This section consists of 3 members,
1. Company Commander
2. Company Sergeant Major
3. CCT/FAC

## Mission Coordinators, ***0-A***

| ![0-A-Logo](/images/orbat/MC.png) | 
|:--:|
| *0-A Logo* |

The mission cooridnators are the zeus team of the unit. They handle the overall flow of the mission with the mission creator and make sure that the platoon is kept on it's toes for any ambushes, attacks, or intel.

They also sometimes role-play as characters to add a human touch to the mission to make the mission more interactable.

## Support Assets

### 1-5-Hawkeye, Aviation

| ![1-5-Hawkeye-Logo](/images/orbat/1_0_Hawkeye.png) | 
|:--:|
| *1-5-Hawkeye Logo* |

The flyboys of the unit, flying everything from drones to fixed wing aircraft. Rotary aircraft include the Hatchet H-60, Official AH-64D Project, MH/AH-6M Littlebirds, CH-47 Chinooks, Tarus and CH-53E Sea Stallions.
Fixed wing currently includes the Tucano and Harriers.
The Falcon, Stomper and Yabhon are some of the most used drones by Hawkeye.

Consists of 6 pilots,
1. Flight Sergeant
2. Sergeant
3. Pilot
4. Pilot
5. Pilot
6. RPA Pilot, Drone specialist

### 1-5-F, Combat Support

| ![1-5-F-Logo](/images/orbat/1_0_Molar.png) | 
|:--:|
| *1-5-F Logo* |

1-5-F is responsible for the following,
* Establish and maintain effective and secure forward supply point for Infantry, Armour and Air
* Provide mobile repair and resupply as required for all combat units
* Establish and maintain a secure fire support position at location designated by IDF troop to support infantry operations
* Cover platoon rear and flanks if required
* Provide infantry combat support if so ordered
* Provide capability for tactical placement of anti-vehicle and anti-personnel ordnance
* Provide EOD support for infantry and armour
* When required, provide secure and viable fall-back position for ground troops
* Provide safe and secure landing LOC for Hawkeye

Section consists of three members,
1. Section Commander
2. Supply Tech
3. Supply Tech

## 1-1 Platoon

| ![1-1-HQ-Logo](/images/orbat/1_1_HQ.png) | 
|:--:|
| *1-1-HQ Logo* |

1-1 Platoon is managed by **1-1 Platoon HQ**. They liaison with _Company HQ_ and _Support Assets_ for the platoon MOVREQS, OPDEMS of Alpha, Bravo and Charlie Sections. **1-1 Platoon HQ** carries out the orders as stated by _Company HQ_ to the best of their abilites and maintain overall structure of the platoon.

1-1 Platoon HQ consists of,
1. Platoon Commander
2. Platoon Sergeant
3. TACP/FAC
4. Platoon Medic
5. Platoon Medic

### 1-1 Alpha

| ![1-1-Alpha-Logo](/images/orbat/1_1_Alpha.png) | 
|:--:|
| *1-1-Alpha Logo* |

Assault specialised section of the platoon. Takes point when moving towards any objective. Consists of two fireteams and a spotter team which can operate independently when required.

1-1 Alpha composition,
1. Section Commander
2. Medic
3. Team Leader (2IC)
4. LMG
5. Marksman
6. Rifleman/LAT
7. Team Leader (3IC)
8. Sapper
9. Grenadier
10. Rifleman
11. Sniper
12. Spotter

### 1-1 Bravo

| ![1-1-Bravo-Logo](/images/orbat/1_1_Bravo.png) | 
|:--:|
| *1-1-Bravo Logo* |

Forward reconnaissance section with majority of members specialising in medical aid. Most of the members are multi-disciplinary specialsing in different munitions. Consists of 2 fireteams and 2 flight medics who can parachute from helicopters into danger zones to provide medical aid.

1-1 Bravo composition,
1. Section Commander
2. Medic
3. Team Leader (2IC)
4. LMG
5. Grenadier
6. Marksman
7. Team Leader (3IC)
8. Sapper
9. Rifleman
10. Rifleman
11. Flight Medic
12. Fligth Medic

### 1-1 Charlie

| ![1-1-Bravo-Logo](/images/orbat/1_1_Charlie.png) | 
|:--:|
| *1-1-Charlie Logo* |

Heavy weapons section in-charge of all heavy and medium anti-tank munitions, medium to heavy armoured vehicles and anti-air munitions. Consists of 2 fireteams, a JTAC and a electronic warfare operative (EWOP).

1-1 Charlie composition,
1. Section Commander
2. Medic
3. Team Leader (2IC)
4. Grenadier
5. MMG
6. Rifleman LAT
7. Team Leader (3IC)
8. Grenadier
9. Rifleman
10. LMG
11. JTAC
12. EWOP