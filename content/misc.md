+++
title = "207 Miscellaneous"
description = "Miscellaneous stuff regarding the unit such as time of trainings, operations and modsets"
date = "2023-02-17"
aliases = ["misc", "misc-207",]
author = "J5 Media"
mermaid = true
+++

## Operational Calendar

The calender contains all the trainings and operations conducted by Detachment 207 personnel.

<div id="207_calendar">
    <iframe src="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=Asia%2FKolkata&showTz=1&showPrint=1&showDate=1&showTitle=1&src=ZGV0YWNobWVudDIwN2o1cHJAZ21haWwuY29t&color=%23039BE5" style="border:solid 1px #777" width="1100" height="600" frameborder="0" scrolling="no"></iframe>
</div>

<script type="text/javascript">
const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone

const html = `<iframe src="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=${timezone}&showTz=1&showPrint=1&showDate=1&showTitle=1&src=ZGV0YWNobWVudDIwN2o1cHJAZ21haWwuY29t&color=%23039BE5" style="border:solid 1px #777" width="1100" height="600" frameborder="0" scrolling="no"></iframe>`

console.log(timezone); // Writing log to console
document.getElementById('207_calendar').innerHTML = html;
</script>

## Modsets

There are *two modsets* for Detachment 207, one for the training server and the other for the operation server. There is a considerable overlap between the modsets, the only differences being the map and one or two extra mods for the operation.

The modsets can be downloaded from the **modset** channel in Detachment 207's discord server.

### Training Server Modset

<iframe src="/modsets/207_Training_Server_Modset_V16.html" scrolling="yes" frameborder="1" style="border: 0" width="1100" height="600"></iframe>

### Current Campaign Modset

<iframe src="/modsets/207_Operation_Hera_V2.html" scrolling="yes" frameborder="1" style="border: 0" width="1100" height="600"></iframe>