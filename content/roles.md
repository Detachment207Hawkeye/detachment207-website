+++
title = "207 Operational Roles"
description = "Operational Roles of Detachment 207 all the way from Company HQ till Individual Platoons"
date = "2023-02-16"
aliases = ["about-us", "about-207", "contact"]
author = "J5 Media"
mermaid = true
+++

## Company HQ, **1-0-HQ**

{{< mermaid >}}
flowchart TD
    id1(Company Commander)-->id2(Company Sergeant Major);
    id1(Company Commander)-->id3(CCT/FAC);
{{< /mermaid >}}
-

### 1-5 Hawkeye, Aviation 

{{< mermaid >}}
flowchart TD
    id1(Flight Sergeant)-->id2(Sergeant);
    id1(Flight Sergeant)-->id3(Pilot);
    id1(Flight Sergeant)-->id4(Pilot);
    id1(Flight Sergeant)-->id5(Pilot);
    id1(Flight Sergeant)-->id6(RPA Pilot, Drone Specialist);
{{< /mermaid >}}
-

### 1-5-F, Combat Support

{{< mermaid >}}
flowchart TD
    id1(Section Commander)-->id2(Supply Tech);
    id1(Section Commander)-->id3(Supply Tech);
{{< /mermaid >}}
-

## Platoon HQ, **1-1-HQ**

{{< mermaid >}}
flowchart TD
    id1(Platoon Commander)-->id2(Platoon Sergeant);
    id1(Platoon Commander)-->id3(TACP/FAC);
    id1(Platoon Commander)-->id4(Platoon Medic);
    id1(Platoon Commander)-->id5(Platoon Medic)
{{< /mermaid >}}
-

### 1-1 Alpha

{{< mermaid >}}
flowchart TD
    id1(Section Commander)-->id3(Team Leader 2IC);
    id1(Section Commander)-->id4(Team Leader 3IC);
    id1(Section Commander)-->id2(Medic);
    id1(Section Commander)-->id5(Sniper);
    id5(Sniper)-->id6(Spotter);
    id3(Team Leader 2IC)-->id7(MMG);
    id3(Team Leader 2IC)-->id8(Marksman);
    id3(Team Leader 2IC)-->id9(Rifleman/LAT);
    id4(Team Leader 3IC)-->id10(Sapper);
    id4(Team Leader 3IC)-->id11(Grenadier);
    id4(Team Leader 3IC)-->id12(LMG);
{{< /mermaid >}}
-

### 1-1 Bravo

{{< mermaid >}}
flowchart TD
    id1(Section Commander)-->id3(Team Leader 2IC);
    id1(Section Commander)-->id4(Team Leader 3IC);
    id1(Section Commander)-->id2(Medic);
    id1(Section Commander)-->id5(Flight Medic);
    id5(Flight Medic)-->id6(Flight Medic);
    id3(Team Leader 2IC)-->id7(LMG);
    id3(Team Leader 2IC)-->id8(Grenadier);
    id3(Team Leader 2IC)-->id9(Marksman);
    id4(Team Leader 3IC)-->id10(Sapper);
    id4(Team Leader 3IC)-->id11(Rifleman);
    id4(Team Leader 3IC)-->id12(Rifleman);
{{< /mermaid >}}
-

### 1-1 Charlie

{{< mermaid >}}
flowchart TD
    id1(Section Commander)-->id3(Team Leader 2IC);
    id1(Section Commander)-->id4(Team Leader 3IC);
    id1(Section Commander)-->id2(Medic);
    id1(Section Commander)-->id5(JTAC);
    id5(JTAC)-->id6(EWOP);
    id3(Team Leader 2IC)-->id7(Grenadier);
    id3(Team Leader 2IC)-->id8(MMG);
    id3(Team Leader 2IC)-->id9(Rifleman/LAT);
    id4(Team Leader 3IC)-->id10(Grenadier);
    id4(Team Leader 3IC)-->id11(Rifleman);
    id4(Team Leader 3IC)-->id12(LMG);
{{< /mermaid >}}
-