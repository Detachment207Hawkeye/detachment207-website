+++
title = "207 Orbat"
description = "Operational Structure of Detachment 207 all the way from Company HQ till Individual Platoons"
date = "2023-02-16"
aliases = ["about-us", "about-207", "contact"]
author = "J5 Media"
mermaid = true
+++

Link to the orbat with section names and personnel can be found [here](https://docs.google.com/spreadsheets/d/1QTxxMIrmSuWlbyh7tohaC3vwH14rQMfZAzGOQlbRXMA/edit#gid=2020084376).

## Structure

{{< mermaid >}}
flowchart TD
    id1(Company HQ, 1-0-HQ)-->id2(Support Assets);
    id1(Company HQ, 1-0-HQ)-->id3(1-1 Platoon, 1-1-HQ);
    id2(Support Assets)-->id4(Aviation, 1-5-Hawkeye);
    id2(Support Assets)-->id5(Combat Support, 1-5-F);
    id3(1-1 Platoon, 1-1-HQ)-->id6(1-1 Alpha);
    id3(1-1 Platoon, 1-1-HQ)-->id7(1-1 Bravo);
    id3(1-1 Platoon, 1-1-HQ)-->id8(1-1 Charlie);

    id9(Mission Coordination 0-A)
{{< /mermaid >}}
-

## Company HQ, **1-0-HQ**

{{< display img="/img/orbat/1_COY_HQ.png" >}}

Company HQ is responsible for handling the platoon(s) as well as the support assets. They also coordinate with the Mission Coordination team to ensure a smooth flow for operations in-game. 

## Mission Coordinators, ***0-A***

{{< display img="/img/orbat/MC.png" >}}

The mission cooridnators are the zeus team of the unit. They handle the overall flow of the mission with the mission creator and make sure that the platoon is kept on it's toes for any ambushes, attacks, or intel.

They also sometimes role-play as characters to add a human touch to the mission to make the mission more interactable.

## Support Assets

### 1-5-Hawkeye, Aviation

{{< display img="/img/orbat/1_0_Hawkeye.png" >}}

The flyboys of the unit, flying everything from drones to fixed wing aircraft. Rotary aircraft include the Hatchet H-60, Official AH-64D Project, MH/AH-6M Littlebirds, CH-47 Chinooks, Tarus and CH-53E Sea Stallions.
Fixed wing currently includes the Tucano and Harriers.
The Falcon, Stomper and Yabhon are some of the most used drones by Hawkeye.

### 1-5-F, Combat Support

{{< display img="/img/orbat/1_0_Molar.png" >}}

1-5-F is responsible for the following,
* Establish and maintain effective and secure forward supply point for Infantry, Armour and Air
* Provide mobile repair and resupply as required for all combat units
* Establish and maintain a secure fire support position at location designated by IDF troop to support infantry operations
* Cover platoon rear and flanks if required
* Provide infantry combat support if so ordered
* Provide capability for tactical placement of anti-vehicle and anti-personnel ordnance
* Provide EOD support for infantry and armour
* When required, provide secure and viable fall-back position for ground troops
* Provide safe and secure landing LOC for Hawkeye

## 1-1 Platoon

{{< display img="/img/orbat/1_1_HQ.png" >}}

1-1 Platoon is managed by **1-1 Platoon HQ**. They liaison with _Company HQ_ and _Support Assets_ for the platoon MOVREQS, OPDEMS of Alpha, Bravo and Charlie Sections. **1-1 Platoon HQ** carries out the orders as stated by _Company HQ_ to the best of their abilites and maintain overall structure of the platoon.

### 1-1 Alpha

{{< display img="/img/orbat/1_1_Alpha.png" >}}

Assault specialised section of the platoon. Takes point when moving towards any objective. Consists of two fireteams and a spotter team which can operate independently when required.

### 1-1 Bravo

{{< display img="/img/orbat/1_1_Bravo.png" >}}

Forward reconnaissance section with majority of members specialising in medical aid. Most of the members are multi-disciplinary specialsing in different munitions. Consists of 2 fireteams and 2 flight medics who can parachute from helicopters into danger zones to provide medical aid.

### 1-1 Charlie

{{< display img="/img/orbat/1_1_Charlie.png" >}}

Heavy weapons section in-charge of all heavy and medium anti-tank munitions, medium to heavy armoured vehicles and anti-air munitions. Consists of 2 fireteams, a JTAC and a electronic warfare operative (EWOP).