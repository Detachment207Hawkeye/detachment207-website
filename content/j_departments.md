+++
title = "207 J Departments"
description = "Structure of the various J Departments within 207 along with their training documents"
date = "2023-05-01"
aliases = ["j_departments", "j1", "j2", "j3", "j4", "j5", "j6"]
author = "J5 Media"
mermaid = true
+++

The J Departments handle the recruitment, *J1*; the mission making and mod-testing/making, *J2*; training for roles, *J3*;  server maintanenance, *J4*; and finally media, J5.

## J1, Recruitment

*J1* handles all the new members within the discord. They guage interest and help the new member into the unit. They conduct the interviews and also welcome the member into the unit. Until new members can attend the Sunday operations, J1 members are usually the points of contacts for any doubts.

This department is also the most professional speaking department when it as they are literally and figuratively the face of the unit and set expectations.

## J2, Mission Making

*J2* handles the mission making for campaigns, mod testing and if possible making mods for a campaign or the community itself. 

The settings for each mod are tested with relevant members to see how each affects gameplay and correspondingly changed. J2 keeps tabs on them as new mods are introduced into the modsets.

## J3, Training

*J3* handles the training for each role all the way from Basic Combat Training to Specialised roles such as medical, pilot or sapper.

The members of J3 post in `trainings` channel on discord for any new training along with the relevant training material required during the training.

<ul id="myUL">
  <li><span class="caret">J3 Departments</span>
    <ul class="nested">
        <li>Basic Combat Training(BCT)</li>
        <li><span class="caret">Medical</span>
            <ul class="nested">
              <li><span class="caret">Combat Life Saver</span>
                <ul class="nested">
                    <li><a href="/j3docs/Medical/CombatMedical/Intro-to-Combat-Medical-LiveFire-Medical-Training-Course.pptx" target="#">Intro to Combat Medical</a></li>
                    <li><a href="/j3docs/Medical/CombatMedical/Combat-Medical-LiveFire-Medical-Training-Course.pptx" target="#">Combat Medical Live Fire</a></li>
                </ul>
              </li>
              <li><a href="/j3docs/Medical/AdvancedLifesaving/Advanced-Lifesaver-LiveFire-Medical-Training-Course.pptx" target="#">Advanced Life Saver</a></li>
              <li>Specialist Life Saver</li>
            </ul>
        </li>
        <li><a href="/j3docs/CQB.zip" target="#">Close Quaters Battle(CQB)</a></li>
        <li><a href="/j3docs/Pronto(Radios).zip" target="#">Pronto(Radios)</a></li>
        <li><a href="/j3docs/Molar.zip" target="#">Molar</a></li>
        <li><a href="/j3docs/Mortars/207_ A_SQN _ 1-0-Odin-Mortars.docx" target="#">Motars</a></li>
        <li><span class="caret">Sapper</span>
            <ul class="nested">
                <li><a href="/j3docs/Sapper/Detachment-207-Training-Document-Sapper-Training.pptx" target="#">Sapper Training</a></li>
                <li><a href="/j3docs/Sapper/Explosive-Familarity-Updated.pptx" target="#">Explosive Formality</a></li>
            </ul>
        </li>
        <li><a href="https://hawkeye.gitbook.io/detachment207hawkeye/" target="#">Pilot</a></li>
        <li><a href="/j3docs/Air-Assault/Air-Assault-Training.docx" target="#">Air Assault</a></li>
        <li><a href="/j3docs/Convoy-Training.zip" target="#">Convoy Training</a></li>
        <li><a href="/j3docs/NCO.zip" target="#">NCO</a></li>
        <li><a href="/j3docs/PL-COMD.zip" target="#">PL COMD</a></li>
        <li><a href="/j3docs/CCT.zip" target="#">Continued Combat Training(CCT)</a></li>
        <li><a href="/j3docs/VehicleIdentification/Tank-identification-guide.docx" target="#">Vehicle Identification</a></li>
    </ul>
  </li>
</ul>

## J4, Server Tech

J4 handles the main server which takes care of all the Arma 3 server and also the Teamspeak server. J4 makes sure everything handles perfectly without any hassles.

## J5, Media

*J5* handles the media aspect of Detachment 207. J5 arranges photos for publicity, maintains this very website, a reddit handle for recruitment, discord handles for recruitment from discord servers, maintains `showcase-media` channel on Detachment 207 discord via members posts. J5 also has a [twitter handle](https://twitter.com/Det207Arma3) and a [facebook page](https://www.facebook.com/Detachment207ARMA).