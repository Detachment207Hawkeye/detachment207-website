+++
author = "J5 Team, Det 207"
title = "Campaign Artemis"
image = "img/Operation_Artemis_Start.jpg"
date = "2023-02-02T00:00:00"
description = "Following the success of Campaign Onyx and the death of Achmed, Detachment 207 is going to the Kujari region (MGR 36P) to report on The Corporation Activities"
tags = [
    "campaigns"
]
+++

## Build-Up

Intel recovered from Achmed's bunker in Vidda and the nuclear explosion from a year ago point towards the region of Kujari, MGR 36P, in Africa. The Corporation which is the military arm of the organisation which Detachment 207 is pursuing is planning something big in Kujari and moving equipment and personnel through the region.

Detachment 207's task is to gather intel regarding The Corporation and assess what is happening there.

## Operation Artemis I

![Figure 1](/img/campaign_artemis/artemis_1.jpg "Detachment 207 Logi Coming In")
|:--:|
| *Detachment 207 Logi Coming In* |

Detachment 207 have been airlifted into the region's northern airfield and will be conducting an armed reconnaissance convoy through the region asking the civilians about any suspicious activity and keep an eye out on any militia or Corporation Forces.

### Aftermath

Operation Artemis 1 saw 207 encounter Kujari insurgents, attempting to destabilise the local region for an unknown reason. No Corporation activities have been discovered at this time.

## Operation Artemis II

Detachment 207 will be inspecting a dirt airstrip in the southern region of Kujari. There is known suspicious activity on the airstrip, but the ownership is a mystery. These flights may be smuggling operations for the Corporation and will expose their activities.

After securing the airfield, a sweep will be conducted for any actionable intelligence. 

### Aftermath

Operation Artemis II saw Detachment 207 take the fight to the Kujari Insurgents. Insurgent forces are confirmed to be working with The Corporation in the development of advanced chemical agents, presumbly to be used in warfare. 

The airfield was captured by Det 207, where a Stomper UGV and it's control pod were discovered in the cargo bay of a C-130.

![Figure 1](/img/campaign_artemis/stomper_image.png "207 member with a stomper")
|:--:|
| *207 member with a stomper* |

## Operation Artemis III

![Figure 1](/img/campaign_artemis/asking_intel.jpg "207 asking for intel")
|:--:|
| *207 asking for intel* |

Detachment 207 will be inserted deep into enemy territory to reconnoiter several buildings identified as potential locations of a commander or leader of the Kujari insurgents. It is suspected that they may hold key intel regarding the The Corporation.

### Aftermath

The HVT was captured by 207 forces after an eventful FRIES insertion. While capturing the HVT, Detachment 207 ran into communication interference which resulted in a distortion of communication to the AO but not away from it. Short range radios had to be switched out by Hawkeye with help of Platoon HQ.

After the capture of the HVT, 207 retreated back to base which was under fire and successfully pushed the attackers back. After the defense, 207 went on the offensive into the town of Marwey to clear out any enemy combatants.

Intel gleaned from the HVT was non-substantial and will be processed by the ASIS for any further intel regarding The Corporation activities.

## Implications

All the intel gleaned from Corporation Activites, points to Corporation in the Florida Keys. With permission from the US government, 207 will be deploying there with all their gear to figure out why are the industrial centres in Florida so important.

<!-- {{< slider src="/data/slide.json" >}} -->