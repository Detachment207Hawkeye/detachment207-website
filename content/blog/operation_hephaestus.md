+++
author = "J5 Team, Det 207"
title = "Campaign Hephaestus"
image = "img/operation_hephaestus/1.png"
date = "2023-02-23"
description = "Following the operations in Kujari under Campaign Artemis Detachment 207 is being sent to Sugar Lake based upon intel retreived in the field. 207 will be tapping into the industrial centres there to figure out why the Corporation is there."
tags = [
    "campaigns"
]
+++

## Build-Up

Based upon intelligence recovered in Operation Artemis II, supported by the HVT captured in the previous campaign has led to Sugar Lake in southern United States of America. Investigation from local authorities into bribery, blackmail, extortion, and running a company town has led to a dead-end as all contact was lost with the authorities shortly after entering the region. 

Graystone PMC which was encountered in the previous campaign are known to be operating within the Sugar Lake area. They are backed by wealthy shareholders and frequently purchase surplus US military equipment and weapons. 

## Operation Hephaestus I

![Figure 1](/img/operation_hephaestus/6.jpg "Detachment 207 Infiltrating via Kayaks")

Detachment 207 has been inserted deep into the Sugar Lake bayou network. 

207 will be splitting up into two to carry out their objectives,
1. Move to Tidewater Complex and investigate local movements
2. Plant explosives on a set of illegal oil pumpjacks and denote upon order.

Hawkeye will be conducting reconnaissance on two areas while maintaining overwatch over the company.

### Aftermath

After engaging Greywater PMC and clearing the Tidewater complexes, D-207 has captured much of the region’s chemical production facilities. Unfortunately, the detonation of the Sugar Lake pumpjacks has destroyed that potential bargaining chip and heavily polluted the western waterways. This has also forced the closure of our resupply corridor. Fortunately, 3 SDVs were delivered before the corridor closed.

## Operation Hephaestus II

{{< display img="/img/operation_hephaestus/2.png" >}}

Despite this precarious situation, D-207 needs to continue with taskings. We have confirmed Graystone PMC, utilising a version of MARPAT-WD camouflage and US armed forces-styled weaponry, is operating in force in the region. We have not confirmed what they were producing or the status of the missing law enforcement personnel. We were also unable to identify any signs of civilians in the region.

D-207 will be undertaking simultaneous actions deep into the Sugar Lake region,
1. Hawkeye will be landing Barvo on Genevieve Island, approximately 500m south of Little Panama. Bravo will be sweeping the town and will sweep the remainder the remainded of the island if no civilian populace is found.
2. Hawkeye will lift two SDVs manned by Alpha to LZ Bromine over the polluted waters.
3. Charlie will mount in SDVs are secure LZ Carbon for 1-5-F Combat Support. Combat Support will load UGVs with explosives at the FOB.
4. Hawkeye will then lift both UGVs (with Combat Support) to LZ Carbon, and Charlie will escort them to OBJ Baker (GR 044971). Combat will set and detonate explosives charges to disable the levee at Baker. Once completed, Combat support is to withdraw to FOB. Charlie will remain onsite and aid Hawkeye where possible in reconnaissance of Camp Burnside (OBJ Dog, GR 053967). Do not approach camp and maintain stealth.